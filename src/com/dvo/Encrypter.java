package com.dvo;

import java.lang.reflect.Field;

/**
 * Created by Danyl on 013 13.07.17.
 */
public class Encrypter {

    public static void encrypt(User user) throws NoSuchFieldException, IllegalAccessException {
        Field fields[] = user.getClass().getDeclaredFields();
        String s = new String();
        String resultString = "";
        for (Field a : fields) {
            if (a.isAnnotationPresent(Encrypt.class)) {
                a.setAccessible(true);
                s = a.get(user).toString();
                for (int i = 0; i < 128; i++) {
                    s = s.replace((char)i,(char)(i-5));
                }
                a.set(user,s);
            }
        }
    }
}
