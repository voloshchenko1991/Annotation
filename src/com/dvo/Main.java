package com.dvo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException {
        String correctPass = "\\a^++^`.3_";
        User myUser = new User();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter user name: ");
        myUser.setUserName(scanner.nextLine());
        System.out.println("Enter password: ");
        myUser.setPassword(scanner.nextLine());
        try {
            Encrypter.encrypt(myUser);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if(myUser.getUserPassword().equals(correctPass)){
            System.out.println("Password is correct!");
        }else{
            System.out.println("Password is invalid!");
        }
    }
}
