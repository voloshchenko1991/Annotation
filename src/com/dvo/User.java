package com.dvo;

/**
 * Created by Danyl on 013 13.07.17.
 */

public class User {

    private String userName;
    @Encrypt
    private String userPassword;


    public void setUserName(String userName){

        this.userName = userName;
    }
    public void setPassword(String userPassword){

        this.userPassword = userPassword;
    }
    public String getUserPassword(){
        return this.userPassword;
    }
}
